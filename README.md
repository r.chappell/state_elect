# Queensland State Election Dashboard

An interactive way for users to explore demographics and political statistics (including finance) at the state electoral level. This dashboard is being built for the Policy Innovation Data Lab at Griffith University. 